<?php

/*
Plugin Name: Test Submission Form
Author: Chris Donalds
Author URI: https://www.codedragon.ca
Description:  A custom form that will be printed on frontend through a shortcode
Version: 1.0.0
License: GPLv3 or later
Text Domain: test-1-submission-form
Domain Path: /languages
*/

if(!defined('CD_FORM_ROOT'))            define('CD_FORM_ROOT', plugin_dir_path(__FILE__));
if(!defined('CD_FORM_URL'))             define('CD_FORM_URL', plugin_dir_url(__FILE__));
if(!defined('CD_FORM_TEXT'))            define('CD_FORM_TEXT', 'text');
if(!defined('CD_FORM_TEXTAREA'))        define('CD_FORM_TEXTAREA', 'textarea');
if(!defined('CD_FORM_EMAIL'))           define('CD_FORM_EMAIL', 'email');
if(!defined('CD_FORM_NUMBER'))          define('CD_FORM_NUMBER', 'number');
if(!defined('CD_FORM_FILE'))            define('CD_FORM_FILE', 'file');

include_once('inc/shortcodes.php');
include_once("inc/filters.php");
include_once("inc/actions.php");

/**
 * Run plugin activation tasks
 */
function cd_form_activation() {
    global $wpdb;

    $sql = "SHOW TABLES FROM `" . DB_NAME . "` LIKE '" . $wpdb->prefix . "cd_form_%';";
    $tables = $wpdb->get_results($sql, ARRAY_A);
    $tables_ok = (!empty($tables)); // initially ok... we'll check for each table though
    $req_tables = array(
        $wpdb->prefix . 'cd_form_entries'
    );

    foreach($tables as $table_elem){
        $table = reset($table_elem);
        if($key = array_search($table, $req_tables)){
            if(false !== $key) unset($req_tables[$key]);    // reduce req_tables if table found
        }
    }

    if(!empty($req_tables)) {
        // one of the required tables was not found in the DB
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE `" . $wpdb->prefix . "cd_form_entries` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `entry_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  `first_name` varchar(50) NOT NULL,
                  `last_name` varchar(50) NOT NULL,
                  `email` varchar(50) NOT NULL,
                  `subject` varchar(50) NOT NULL,
                  `message` text NOT NULL,
                  PRIMARY KEY  (id)
                ) $charset_collate;";
        dbDelta( $sql );
    }
}
register_activation_hook( __FILE__, 'cd_form_activation' );

/**
 * Return excerpt truncated to nearest word within limit
 * @param  integer $limit
 * @return string
 */
function cd_form_get_excerpt($limit = 250){
    global $post;

    $limit = intval($limit);
    $excerpt = get_the_excerpt();
    $excerpt = substr($excerpt, 0, $limit);
    $result = substr($excerpt, 0, strrpos($excerpt, ' '));
    return $result;
}

/**
 * Get page by slug respecting polylang
 * @param  string $slug
 * @param  string $lang_slug
 * @param  string $type
 * @return string
 */
function cd_form_get_link_by_slug($slug, $lang_slug = null, $type = 'post'){
    $post = get_page_by_path($slug, OBJECT, $type);
    $id = ($lang_slug) ? pll_get_post($post->ID, $lang_slug) : $post->ID;
    return get_permalink($id);
}

/**
 * Return array element or default if not exists
 * @param  array &$array
 * @param  string $element
 * @param  mixed $default
 * @return mixed
 */
function cd_form_get_if_set($array, $element, $index = null, $default = null){
    $data = $default;
    if(isset($array[$element])) {
        $data = $array[$element];
        if(!is_null($index) && isset($data[$index]))
            $data = $data[$index];
    }
    return $data;
}

/**
 * Stylized print_r()
 * @param  array  $array
 */
function cd_form_pr($array, $title = '', $silent = false){
    if($silent){
        print "<!-- " . $title . ": " . print_r($array, true) . " -->";
    }else{
        print "<pre>" . $title . ": " . print_r($array, true) . "</pre>";
    }
}
