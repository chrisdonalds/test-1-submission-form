jQuery(document).ready(function($){
	$('input[type="url"]').blur(function(){
		if($(this).val().substr(0, 4) == 'www.') $(this).val('https://' + $(this).val());
	});

	$('.form-test-entry-view').click(function(e){
		e.preventDefault();

		var id = $(this).data('id');
		if(id > 0){
	        $.ajax({
	            type: 'POST',
	            url: cd_form_ajax_obj.ajaxurl,
	            dataType: 'json',
	            data: {
	                'action': 'cd_form_ajax_request',
	                'data': {id: id},
	                'request': 'form_entry_view',
	                'nonce': cd_form_ajax_obj.nonce,
	            },
	            success: function(result, status, xhr) {
	            	if(result.success == false){
	            		$('.form-test-view').html('<p>The entry data was not found.</p>');
	            	}else{
	            		$('.form-test-view').html(result.data);
	            	}
	            },
	            error: function(xhr, status, error) {
	                console.log("Search failed.");
	                console.log(xhr);
	                console.log(status);
	                console.log(error);
	                setTimeout(function(){
	                    $('.srch-overlay').removeClass('active');
	                }, 400);
	            }
	        });
	    }
	});

	$('.form-test-entry-page').click(function(e){
		e.preventDefault();

		var offset = $(this).data('offset');
		if(offset > 0){
	        $.ajax({
	            type: 'POST',
	            url: cd_form_ajax_obj.ajaxurl,
	            dataType: 'json',
	            data: {
	                'action': 'cd_form_ajax_request',
	                'data': {offset: offset},
	                'request': 'form_entries_refresh',
	                'nonce': cd_form_ajax_obj.nonce,
	            },
	            success: function(result, status, xhr) {
	            	if(result.success != false){
	            		$('.form-test-box').html(result.data);
	            	}
	            },
	            error: function(xhr, status, error) {
	                console.log("Search failed.");
	                console.log(xhr);
	                console.log(status);
	                console.log(error);
	                setTimeout(function(){
	                    $('.srch-overlay').removeClass('active');
	                }, 400);
	            }
	        });
	    }
	});
});