jQuery(document).ready(function($){
	$('.form-test').submit(function(e){
		var ok_to_submit = true;
		$('.form-test input, .form-test textarea, .form-test select').each(function(){
			if($(this).hasClass('reqd') && $(this).val() == ''){
				var label = $(this).closest('.form-field').find('label').text().replace('*', '');
				alert('The `' + label + '` field is required.');
				ok_to_submit = false;
			}
		})

		if(ok_to_submit){
			var form_data = $('.form-test').serialize();
	        $.ajax({
	            type: 'POST',
	            url: cd_form_ajax_obj.ajaxurl,
	            dataType: 'json',
	            data: {
	                'action': 'cd_form_ajax_request',
	                'data': form_data,
	                'request': 'form_sub',
	                'nonce': cd_form_ajax_obj.nonce,
	            },
	            success: function(result, status, xhr) {
	            	if(result.success == false){
	            		alert(result.data);
	            	}else{
	            		$('.form-test').parent().html('<p>Thank you for sending us your feedback</p>');
	            	}
	            },
	            error: function(xhr, status, error) {
	                console.log("Search failed.");
	                console.log(xhr);
	                console.log(status);
	                console.log(error);
	                setTimeout(function(){
	                    $('.srch-overlay').removeClass('active');
	                }, 400);
	            }
	        });
    		return false;
	    }else{
	    	return false;
	    }
    });

	$('.form-test-box').on('click', '.form-test-entry-view', function(e){
		e.preventDefault();

		var id = $(this).data('id');
		$('.form-test-overlay').show();
		if(id > 0){
	        $.ajax({
	            type: 'POST',
	            url: cd_form_ajax_obj.ajaxurl,
	            dataType: 'json',
	            data: {
	                'action': 'cd_form_ajax_request',
	                'data': {id: id},
	                'request': 'form_entry_view',
	                'nonce': cd_form_ajax_obj.nonce,
	            },
	            success: function(result, status, xhr) {
	            	if(result.success != false){
	            		$('.form-test-view').html(result.data);
	            	}
					$('.form-test-overlay').hide();
	            },
	            error: function(xhr, status, error) {
	                console.log("Search failed.");
	                console.log(xhr);
	                console.log(status);
	                console.log(error);
	                setTimeout(function(){
	                    $('.srch-overlay').removeClass('active');
	                }, 400);
	            }
	        });
	    }
	});

	$('.form-test-paging').on('click', '.form-test-entry-page', function(e){
		e.preventDefault();

		var offset = $(this).data('offset');
		$('.form-test-overlay').show();
        $.ajax({
            type: 'POST',
            url: cd_form_ajax_obj.ajaxurl,
            dataType: 'json',
            data: {
                'action': 'cd_form_ajax_request',
                'data': {offset: offset},
                'request': 'form_entries_refresh',
                'nonce': cd_form_ajax_obj.nonce,
            },
            success: function(result, status, xhr) {
            	if(result.success != false){
            		$('.form-test-body').html(result.data.entries);
            		$('.form-test-paging').html(result.data.paging);
            		$('.form-test-view').html('');
					$('.form-test-overlay').hide();
            	}
            },
            error: function(xhr, status, error) {
                console.log("Search failed.");
                console.log(xhr);
                console.log(status);
                console.log(error);
                setTimeout(function(){
                    $('.srch-overlay').removeClass('active');
                }, 400);
            }
        });
	});
});