<?php

/*
Usage: Shortcodes
Author: Chris Donalds
Author URI: https://www.codedragon.ca
*/

/**
 * Returns the HTML for a message submission form
 * @param  array  $atts
 * @return html
 */
function cd_form_show_form($atts = array()){
	$atts = shortcode_atts(array(
    ), $atts, 'cd-form-show-form');

    $current_user = wp_get_current_user();
    if ( $current_user->ID > 0 ) {
        // Logged in.
    	$user_data = get_userdata( $current_user->ID );
    	$first_name = $user_data->first_name;
    	$last_name = $user_data->last_name;
    	$email = $user_data->user_email;
    } else {
        // Not logged in.
        $first_name = $last_name = $email = null;
    }

	$fields = array(
		'first_name' => array(
			'type' => CD_FORM_TEXT, 'label' => esc_html__('First Name', 'test-1-submission-form'), 'maxlength' => 30, 'default' => $first_name, 'placeholder' => esc_html__('Enter your first name', 'test-1-submission-form'), 'readonly' => false, 'disabled' => false, 'required' => true
		),
		'last_name' => array(
			'type' => CD_FORM_TEXT, 'label' => esc_html__('Last Name', 'test-1-submission-form'), 'maxlength' => 30, 'default' => $last_name, 'placeholder' => esc_html__('...and last name', 'test-1-submission-form'), 'readonly' => false, 'disabled' => false, 'required' => true
		),
		'email' => array(
			'type' => CD_FORM_EMAIL, 'label' => esc_html__('Email', 'test-1-submission-form'), 'maxlength' => 30, 'default' => $email, 'placeholder' => esc_html__('...and your email', 'test-1-submission-form'), 'readonly' => false, 'disabled' => false, 'required' => true
		),
		'subject' => array(
			'type' => CD_FORM_TEXT, 'label' => esc_html__('Subject', 'test-1-submission-form'), 'maxlength' => 30, 'default' => '', 'placeholder' => esc_html__('...the subject', 'test-1-submission-form'), 'readonly' => false, 'disabled' => false, 'required' => true
		),
		'message' => array(
			'type' => CD_FORM_TEXTAREA, 'label' => esc_html__('Message', 'test-1-submission-form'), 'rows' => 4, 'default' => ''
		)
	);

	$html = '<form class="form-test" method="POST" action="">';
	$nonce = wp_create_nonce( 'cd-form-ajax-nonce' );
	$html.= '<input type="hidden" name="n" value="' . $nonce . '" />';
	$reqd = '<span class="red reqd">*</span>';
	foreach($fields as $field_key => $field_meta){
		$reqd_class = (($field_meta['required']) ? ' reqd' : '');
		switch($field_meta['type']){
			case CD_FORM_TEXT:
				$field_html = '<input type="text" class="form-field-input-text' . $reqd_class . '" name="' . $field_key . '" value="' . $field_meta['default'] . '"' . $field_meta['placeholder'] . (($field_meta['readonly']) ? ' readonly="readonly"' : '') . (($field_meta['disabled']) ? ' disabled="disabled"' : '') . ' />';
				break;
			case CD_FORM_TEXTAREA:
				$field_html = '<textarea class="form-field-input-textarea' . $reqd_class . '" name="' . $field_key . '" rows="' . $field_meta['rows'] . '">' . $field_meta['default'] . '</textarea>';
				break;
			case CD_FORM_EMAIL:
				$field_html = '<input type="email" class="form-field-input-email' . $reqd_class . '" name="' . $field_key . '" value="' . $field_meta['default'] . '"' . $field_meta['placeholder'] . (($field_meta['readonly']) ? ' readonly="readonly"' : '') . (($field_meta['disabled']) ? ' disabled="disabled"' : '') . ' />';
				break;
			case CD_FORM_NUMBER:
				$field_html = '<input type="number" class="form-field-input-number' . $reqd_class . '" name="' . $field_key . '" value="' . $field_meta['default'] . '"' . (($field_meta['min']) ? ' min="' . $field_meta['min'] . '"' : '') . (($field_meta['max']) ? ' max="' . $field_meta['max'] . '"' : '') . (($field_meta['readonly']) ? ' readonly="readonly"' : '') . (($field_meta['disabled']) ? ' disabled="disabled"' : '') . ' />';
				break;
			default:
				$field_html = '';
				break;
		}
		if(!empty($field_html)){
			$html.= '<div class="form-field form-field-' . $field_meta['type'] . '">';
			$html.= '<label for"' . $field_key . '">' . $field_meta['label'] . (($field_meta['required']) ? $reqd : '') . '</label><span>' . $field_html . '</span>';
			$html.= '</div>';
		}
	}
	$html.= '<div class="form-field form-field-submit">';
	$html.= '<span><button class="form-field-input-submit">';
	$html.= esc_html__('Send Message', 'test-1-submission-form');
	$html.= '</button></span>';
	$html.= '</div>';
	$html.= '</form>';

	return $html;
}
add_shortcode('cd-form-show-form', 'cd_form_show_form');

/**
 * Returns the HTML for the admin entries list
 * @param  array  $atts
 * @return html
 */
function cd_form_show_admin_entries($atts = array()){
	global $wpdb;

	$atts = shortcode_atts(array(
    ), $atts, 'cd-form-show-admin-entries');

    $current_user = wp_get_current_user();
	if ( $current_user->ID == 0 ) {
		$html = '<p>' . esc_html__('You are not authorized to view the content of this page', 'test-1-submission-form') . '.</p>';
	} else {
		$entries = $wpdb->get_results('SELECT * FROM `' . $wpdb->prefix . 'cd_form_entries` ORDER BY `entry_date` DESC LIMIT 0, 10', ARRAY_A);
		$date_label = esc_html__('Date', 'test-1-submission-form');
		$first_name_label = esc_html__('First Name', 'test-1-submission-form');
		$last_name_label = esc_html__('Last Name', 'test-1-submission-form');
		$email_label = esc_html__('Email', 'test-1-submission-form');
		$subject_label = esc_html__('Subject', 'test-1-submission-form');
		$action_label = esc_html__('Action', 'test-1-submission-form');

		$html = <<<EOT
		<div class="form-test-box">
			<table class="form-test-admin-table widefat fixed">
				<thead>
					<tr>
						<th scope="col" class="form-test-column">{$date_label}</th>
						<th scope="col" class="form-test-column">{$first_name_label}</th>
						<th scope="col" class="form-test-column">{$last_name_label}</th>
						<th scope="col" class="form-test-column">{$email_label}</th>
						<th scope="col" class="form-test-column">{$subject_label}</th>
						<th scope="col" class="form-test-column">{$action_label}</th>
					</tr>
				</thead>
				<tbody class="form-test-body">
EOT;

		$html .= cd_form_get_admin_page_contents($entries);

		$html .= <<<EOT
				</tbody>
			</table>
		</div>
		<div class="form-test-paging">
EOT;

		$html .= cd_form_get_pagination(1);

		$html .= <<<EOT
		</div>
		<div class="form-test-view"></div>
		<div class="form-test-overlay"><div class="loader"></div></div>
EOT;
	}
	return $html;
}
add_shortcode('cd-form-show-admin-entries', 'cd_form_show_admin_entries');

/**
 * Admin page contents
 * @param array $entries
 * @return html
 */
function cd_form_get_admin_page_contents($entries) {
	$html = '';
	foreach($entries as $entry){
		$html .= '<tr>';
		$html .= '<td class="form-test-column-entry_date">' . $entry['entry_date'] . '</td>';
		$html .= '<td class="form-test-column-first_name">' . $entry['first_name'] . '</td>';
		$html .= '<td class="form-test-column-last_name">' . $entry['last_name'] . '</td>';
		$html .= '<td class="form-test-column-email">' . $entry['email'] . '</td>';
		$html .= '<td class="form-test-column-subject">' . substr($entry['subject'], 0, 30) . '</td>';
		$html .= '<td class="form-test-column-action"><a href="" class="form-test-entry-view" data-id="' . $entry['id'] . '">View</a></td>';
		$html .= '</tr>';
	}
	return $html;
}

/**
 * Return the pagination HTML
 * @param  integer $offset
 * @return html
 */
function cd_form_get_pagination($offset = 0){
	global $wpdb;

	$entry_count = $wpdb->get_results('SELECT COUNT(`id`) AS `num` FROM `' . $wpdb->prefix . 'cd_form_entries`', ARRAY_A);
	$html = '';
	for($i=0; $i<$entry_count[0]['num'] / 10; $i++){
		$page = $i + 1;
		$active = (($page == $offset) ? ' active' : '');
		$html .= '<a href="" class="form-test-entry-page' . $active . '" data-offset="' . $page . '">' . $page . '</a>';
	}
	return $html;
}