<?php

/*
Usage: Actions
Author: Chris Donalds
Author URI: https://www.codedragon.ca
*/

/**
 * Enqueue scripts/stylesheets
 */
if ( ! function_exists( 'cd_form_enqueue_scripts' ) ) {
    function cd_form_enqueue_scripts() {
        if(!is_admin()) {
            global $post;

            // scripts
            wp_enqueue_script( 'cd-form-wp-scripts', plugins_url( 'test-1-submission-form/assets/js/scripts.js' ), array(), null );

            // styles
            wp_register_style( 'cd-form-wp-css', plugins_url( 'test-1-submission-form/assets/css/style.css' ) );
            wp_enqueue_style( 'cd-form-wp-css' );

            // AJAX
            wp_localize_script(
                'cd-form-wp-scripts',
                'cd_form_ajax_obj',
                array(
                    'ajaxurl' => admin_url('admin-ajax.php'),
                    'nonce' => wp_create_nonce('cd-form-ajax-nonce')
                )
            );
        }
    }
}
add_action( 'wp_enqueue_scripts', 'cd_form_enqueue_scripts' );

/**
 * Enqueue Font Awesome.
 */
function cd_form_load_admin_scripts($hook) {
    // Load only on ?page=form-entries
    if( $hook != 'toplevel_page_form-entries' ) {
         return;
    }

    wp_register_style( 'cd-form-wp-admin-css', plugins_url( 'test-1-submission-form/assets/css/admin.css' ) );
    wp_enqueue_style( 'cd-form-wp-admin-css' );

    wp_enqueue_script( 'cd-form-wp-admin-scripts', plugins_url( 'test-1-submission-form/assets/js/admin.js' ), array(), null );

    // AJAX
    wp_localize_script(
        'cd-form-wp-admin-scripts',
        'cd_form_ajax_obj',
        array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('cd-form-ajax-nonce')
        )
    );
}
add_action( 'admin_enqueue_scripts', 'cd_form_load_admin_scripts' );

/**
 * Initial tasks
 */
function cd_form_init() {
    load_plugin_textdomain( 'test-1-submission-form', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'init', 'cd_form_init' );

/**
 * Ajax calls
 */
function cd_form_ajax_request() {
    $nonce = $_POST['nonce'];
    if(!is_array($_POST['data'])){
        parse_str($_POST['data'], $data);
    } else {
        $data = $_POST['data'];
    }

    if (!wp_verify_nonce( $nonce, 'cd-form-ajax-nonce')) {
        die('Nonce value cannot be verified.');
    }

    global $wpdb;
    $retn_code = 200;
    $retn_data = '';

    switch($_POST['request']){
        case 'form_sub':
            if(!empty($data['first_name']) && !empty($data['last_name']) && !empty($data['subject']) && !empty($data['email'])) {
                $retn = $wpdb->insert(
                    $wpdb->prefix . 'cd_form_entries',
                    array(
                        'first_name' => $data['first_name'],
                        'last_name' => $data['last_name'],
                        'email' => $data['email'],
                        'subject' => $data['subject'],
                        'message' => $data['message'],
                    ),
                    array(
                        '%s',
                        '%s',
                        '%s',
                        '%s',
                        '%s'
                    )
                );
                if(false === $retn){
                    $retn_data = 'Problem saving form data';
                }
            }
            break;
        case 'form_entry_view':
            $rec = $wpdb->get_results("SELECT * FROM `" . $wpdb->prefix . "cd_form_entries` WHERE `id` = '" . $data['id'] . "' LIMIT 1", ARRAY_A);
            if(!empty($rec)){
                $retn_data = '
                <h3>Entry Information</h3>
                <div class="form-test-entry"><label>' . esc_html__('ID', 'test-1-submission-form') . ':</label><span class="form-test-entry-value">' . $rec[0]['id'] . '</span></div>
                <div class="form-test-entry"><label>' . esc_html__('Date/Time', 'test-1-submission-form') . ':</label><span class="form-test-entry-value">' . $rec[0]['entry_date'] . '</span></div>
                <div class="form-test-entry"><label>' . esc_html__('First Name', 'test-1-submission-form') . ':</label><span class="form-test-entry-value">' . $rec[0]['first_name'] . '</span></div>
                <div class="form-test-entry"><label>' . esc_html__('Last Name', 'test-1-submission-form') . ':</label><span class="form-test-entry-value">' . $rec[0]['last_name'] . '</span></div>
                <div class="form-test-entry"><label>' . esc_html__('Email', 'test-1-submission-form') . ':</label><span class="form-test-entry-value">' . $rec[0]['email'] . '</span></div>
                <div class="form-test-entry"><label>' . esc_html__('Subject', 'test-1-submission-form') . ':</label><span class="form-test-entry-value">' . $rec[0]['subject'] . '</span></div>
                <div class="form-test-entry"><label>' . esc_html__('Message', 'test-1-submission-form') . ':</label><span class="form-test-entry-value">' . $rec[0]['message'] . '</span></div>
                ';
            }
            break;
        case 'form_entries_refresh':
            $offset = intval($data['offset']) * 10 - 9;
            $entries = $wpdb->get_results('SELECT * FROM `' . $wpdb->prefix . 'cd_form_entries` ORDER BY `entry_date` DESC LIMIT ' . $offset . ', 10', ARRAY_A);
            $a = array();
            $a['query'] = 'SELECT * FROM `' . $wpdb->prefix . 'cd_form_entries` ORDER BY `entry_date` DESC LIMIT ' . $offset . ', 10';
            $a['entries'] = cd_form_get_admin_page_contents($entries);
            $a['paging'] = cd_form_get_pagination($data['offset']);
            $retn_data = $a;
            break;
    }
    wp_send_json_success($retn_data, $retn_code);

    die();
}
add_action('wp_ajax_cd_form_ajax_request', 'cd_form_ajax_request');
add_action('wp_ajax_nopriv_cd_form_ajax_request', 'cd_form_ajax_request');
